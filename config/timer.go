package config

type Timer struct {
	Spec        string   `koanf:"spec" json:"spec" yaml:"spec"` // CRON表达式
	Detail      []Detail `koanf:"detail" json:"detail" yaml:"detail"`
	Start       bool     `koanf:"start" json:"start" yaml:"start"`                      // 是否启用
	WithSeconds bool     `koanf:"with_seconds" json:"with_seconds" yaml:"with_seconds"` // 是否精确到秒

}

type Detail struct {
	TableName    string `koanf:"tableName" json:"tableName" yaml:"tableName"`          // 需要清理的表名
	CompareField string `koanf:"compareField" json:"compareField" yaml:"compareField"` // 需要比较时间的字段
	Interval     string `koanf:"interval" json:"interval" yaml:"interval"`             // 时间间隔
}
