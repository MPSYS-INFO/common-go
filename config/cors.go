package config

type CORS struct {
	Mode             string   `koanf:"mode" json:"mode" yaml:"mode"`
	AllowOrigin      []string `koanf:"allow-origin" json:"allow-origin" yaml:"allow-origin"`
	AllowMethods     []string `koanf:"allow-methods" json:"allow-methods" yaml:"allow-methods"`
	AllowHeaders     []string `koanf:"allow-headers" json:"allow-headers" yaml:"allow-headers"`
	ExposeHeaders    []string `koanf:"expose-headers" json:"expose-headers" yaml:"expose-headers"`
	AllowCredentials bool     `koanf:"allow-credentials" json:"allow-credentials" yaml:"allow-credentials"`
	AllowWebSockets  bool     `koanf:"allow-web-sockets" json:"allow-web-sockets" yaml:"allow-web-sockets"`
}
