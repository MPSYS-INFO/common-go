package websocket

type wsMessageType byte

const (
	messageTypeText   wsMessageType = 1
	messageTypeBinary wsMessageType = 2
	messageTypePing   wsMessageType = 9
)

type wsMessage struct {
	Type    wsMessageType
	Payload []byte
}

type transformation interface {
	Bytes() []byte
}

// NewTextMessage initializes a new text wsMessage from bytes.
func NewTextMessage[T transformation](payload T) wsMessage {
	return wsMessage{Type: messageTypeText, Payload: payload.Bytes()}
}

// NewBinaryMessage initializes a new binary wsMessage from bytes.
func NewBinaryMessage(payload []byte) wsMessage {
	return wsMessage{Type: messageTypeBinary, Payload: payload}
}

// NewPingMessage initializes a new ping wsMessage.
func NewPingMessage() wsMessage {
	return wsMessage{Type: messageTypePing}
}
