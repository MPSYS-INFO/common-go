package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

type ClientSendBufferOverflowError struct {
	ID uuid.UUID
}

// ClientSendBufferOverflowError implements an error interface.
func (e *ClientSendBufferOverflowError) Error() string {
	return fmt.Sprintf("wspubsub: client send buffer is full: id=%s", e.ID)
}

// NewClientSendBufferOverflowError initializes a new ClientSendBufferOverflowError.
func NewClientSendBufferOverflowError(id uuid.UUID) *ClientSendBufferOverflowError {
	return &ClientSendBufferOverflowError{ID: id}
}

// IsClientSendBufferOverflowError checks if error type is ClientSendBufferOverflowError.
func IsClientSendBufferOverflowError(err error) (*ClientSendBufferOverflowError, bool) {
	var v *ClientSendBufferOverflowError
	ok := errors.As(err, &v)

	return v, ok
}
