package config

import "strconv"

type AMQP struct {
	Host        string `koanf:"host" json:"host" yaml:"host"`
	Username    string `koanf:"username" json:"username" yaml:"username"`
	Password    string `koanf:"password" json:"password" yaml:"password"`
	Port        int    `koanf:"port" json:"port" yaml:"port"`
	VirtualHost string `koanf:"virtual-host" json:"virtual-host" yaml:"virtual-host"`
}

func (a *AMQP) URI() string {
	return "amqp://" + a.Username + ":" + a.Password + "@" + a.Host + ":" + strconv.Itoa(a.Port) + "/" + a.VirtualHost
}
