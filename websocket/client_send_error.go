package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

// ClientSendError returned when a wsMessage (text or binary) can't be written to a WebSocket connection.
type ClientSendError struct {
	ID      uuid.UUID
	Message wsMessage
	Err     error
}

// ClientSendError implements an error interface.
func (e *ClientSendError) Error() string {
	return fmt.Sprintf("wspubsub: client failed to send a wsMessage: id=%s, err=%s", e.ID, e.Err)
}

// NewClientSendError initializes a new ClientSendError.
func NewClientSendError(id uuid.UUID, message wsMessage, err error) *ClientSendError {
	return &ClientSendError{ID: id, Message: message, Err: err}
}

// IsClientSendError checks if error type is ClientSendError.
func IsClientSendError(err error) (*ClientSendError, bool) {
	var v *ClientSendError
	ok := errors.As(err, &v)

	return v, ok
}
