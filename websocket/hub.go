package websocket

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.uber.org/multierr"
	"golang.org/x/sync/errgroup"
	"net/http"
	"sync/atomic"
	"time"
)

var MPS_HUB *Hub

type (
	// ConnectHandler called when a new client is connected to hub.
	ConnectHandler func(ctx context.Context, clientID uuid.UUID)

	// DisconnectHandler called when a client is disconnected from the hub.
	DisconnectHandler func(ctx context.Context, clientID uuid.UUID)

	// ReceiveHandler called when a client reads a new wsMessage.
	ReceiveHandler func(ctx context.Context, clientID uuid.UUID, message wsMessage)

	// ErrorHandler called when an error occurred when reading or writing messages.
	ErrorHandler func(ctx context.Context, clientID uuid.UUID, err error)
)

type WebsocketClient interface {
	Context() context.Context
	ID() uuid.UUID
	Connect(ctx *gin.Context) error
	OnReceive(handler ReceiveHandler)
	OnError(handler ErrorHandler)
	Send(message wsMessage) error
	Close() error
}

type WebsocketClientStore interface {
	Get(clientID uuid.UUID) (WebsocketClient, error)
	Set(client WebsocketClient)
	Unset(clientID uuid.UUID) error
	Count(channels ...string) int
	Find(fn IterateFunc, channels ...string) error
	Channels(clientID uuid.UUID) ([]string, error)
	CountChannels(clientID uuid.UUID) (int, error)
	SetChannels(clientID uuid.UUID, channels ...string) error
	UnsetChannels(clientID uuid.UUID, channels ...string) error
}

type WebsocketClientFactory interface {
	Create() WebsocketClient
}

var (
	defaultConnectHandler    = ConnectHandler(func(ctx context.Context, clientID uuid.UUID) {})
	defaultDisconnectHandler = DisconnectHandler(func(ctx context.Context, clientID uuid.UUID) {})
	defaultReceiveHandler    = ReceiveHandler(func(ctx context.Context, clientID uuid.UUID, message wsMessage) {})
	defaultErrorHandler      = ErrorHandler(func(ctx context.Context, clientID uuid.UUID, err error) {})
)

type Hub struct {
	options           HubOptions
	clients           WebsocketClientStore
	clientFactory     WebsocketClientFactory
	connectHandler    atomic.Value
	disconnectHandler atomic.Value
	receiveHandler    atomic.Value
	errorHandler      atomic.Value
}

// Subscribe allows to subscribe a client to specific channels.
// At least one channel is required.
func (h *Hub) Subscribe(clientID uuid.UUID, channels ...string) error {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.subscribe: took=%s", end)
			}
		}()
	}

	if len(channels) == 0 {
		return NewHubSubscriptionChannelRequiredError()
	}

	err := h.clients.SetChannels(clientID, channels...)
	if err != nil {
		return errors.WithStack(err)
	}

	if h.options.IsDebug {
		//h.logger.Debugf("Client subscribed: id=%s, channels=[%s]", clientID, strings.Join(channels, ","))
	}

	return nil
}

// Unsubscribe allows to unsubscribe a client from specific channels.
// If channels were not specified then the client will be
// unsubscribed from all channels.
func (h *Hub) Unsubscribe(clientID uuid.UUID, channels ...string) error {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.unsubscribe: took=%s", end)
			}
		}()
	}

	err := h.clients.UnsetChannels(clientID, channels...)
	if err != nil {
		return errors.WithStack(err)
	}

	if h.options.IsDebug {
		//h.logger.Debugf("Client unsubscribed: id=%s, channels=[%s]", clientID, strings.Join(channels, ","))
	}

	return nil
}

// IsSubscribed checks does the client is subscribed to at least one channel.
func (h *Hub) IsSubscribed(clientID uuid.UUID) bool {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.is_subscribed: took=%s", end)
			}
		}()
	}

	count, _ := h.clients.CountChannels(clientID)

	return count > 0
}

// Channels return a list of channels the client currently subscribed to.
func (h *Hub) Channels(clientID uuid.UUID) ([]string, error) {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.channels: took=%s", end)
			}
		}()
	}

	channels, err := h.clients.Channels(clientID)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return channels, nil
}

// Count returns total number of connected clients.
func (h *Hub) Count(channels ...string) int {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.count: took=%s", end)
			}
		}()
	}

	return h.clients.Count(channels...)
}

// Publish publishes a wsMessage to the channels.
// If channels were not specified then all clients will receive the wsMessage.
func (h *Hub) Publish(message wsMessage, channels ...string) (int, error) {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.publish: took=%s", end)
			}
		}()
	}

	numClients := 0
	iterateFunc := func(client WebsocketClient) error {
		err := client.Send(message)
		if err != nil {
			// A buffer overflow error can occur here,
			// so we should disconnect the client
			_ = h.disconnectClient(client)

			return nil
		}

		numClients++

		return nil
	}

	err := h.clients.Find(iterateFunc, channels...)
	if err != nil {
		return numClients, errors.WithStack(err)
	}

	if h.options.IsDebug {
		if numClients > 0 {
			//h.logger.Debugf("wsMessage published: num_clients=%d, channels=[%s]", numClients, strings.Join(channels, ","))
		}
	}

	return numClients, nil
}

// Send sends a wsMessage to a specific client.
func (h *Hub) Send(clientID uuid.UUID, message wsMessage) error {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.send: took=%s", end)
			}
		}()
	}

	client, err := h.clients.Get(clientID)
	if err != nil {
		return errors.WithStack(err)
	}

	err = client.Send(message)
	if err != nil {
		return errors.WithStack(err)
	}

	if h.options.IsDebug {
		//h.logger.Debugf("wsMessage sent: id=%s", clientID)
	}

	return nil
}

// Disconnect closes a client connection and removes it from the storage.
func (h *Hub) Disconnect(clientID uuid.UUID) error {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.disconnect: took=%s", end)
			}
		}()
	}

	client, err := h.clients.Get(clientID)
	if err != nil {
		return errors.WithStack(err)
	}

	err = h.disconnectClient(client)
	if err != nil {
		return errors.WithStack(err)
	}

	if h.options.IsDebug {
		//h.logger.Debugf("Client disconnected: id=%s", clientID)
	}

	return nil
}

// Close shutdowns http-servers and disconnects clients.
func (h *Hub) Close() error {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.close: took=%s", end)
			}
		}()
	}

	//h.logger.Info("Closing connections...")

	eg := errgroup.Group{}

	errList := multierr.Combine(eg.Wait())

	iterateFunc := func(client WebsocketClient) error {
		_ = h.disconnectClient(client)

		return nil
	}

	errList = multierr.Combine(errList, h.clients.Find(iterateFunc))

	return errors.WithStack(errList)
}

// OnConnect registers a handler for client connection.
func (h *Hub) OnConnect(handler ConnectHandler) {
	//h.logger.Infof("Registering handler: %T", handler)
	h.connectHandler.Store(handler)
}

// OnDisconnect registers a handler for client disconnection.
func (h *Hub) OnDisconnect(handler DisconnectHandler) {
	//h.logger.Infof("Registering handler: %T", handler)
	h.disconnectHandler.Store(handler)
}

// OnReceive registers a handler for incoming messages.
func (h *Hub) OnReceive(handler ReceiveHandler) {
	//h.logger.Infof("Registering handler: %T", handler)
	h.receiveHandler.Store(handler)
}

// OnError registers a handler for errors occurred while reading or writing connection.
func (h *Hub) OnError(handler ErrorHandler) {
	//h.logger.Infof("Registering handler: %T", handler)
	h.errorHandler.Store(h.wrapErrorHandler(handler))
}

func (h *Hub) connectClient(client WebsocketClient, ctx *gin.Context) error {
	h.clients.Set(client)

	err := client.Connect(ctx)
	if err != nil {
		_ = h.clients.Unset(client.ID())

		return errors.WithStack(err)
	}

	connectHandler := h.connectHandler.Load().(ConnectHandler)
	connectHandler(client.Context(), client.ID())

	return nil
}

func (h *Hub) disconnectClient(client WebsocketClient) error {
	err := h.clients.Unset(client.ID())
	if err != nil {
		return errors.WithStack(err)
	}

	err = client.Close()
	if err != nil {
		return errors.WithStack(err)
	}

	disconnectHandler := h.disconnectHandler.Load().(DisconnectHandler)
	disconnectHandler(client.Context(), client.ID())

	return nil
}

func (h *Hub) wrapErrorHandler(handler ErrorHandler) ErrorHandler {
	return func(ctx context.Context, clientID uuid.UUID, err error) {
		handler(ctx, clientID, err)

		// We should disconnect the client
		// if it reported (called the error_handler) that
		// an error has occurred while reading or writing a websocket
		_ = h.Disconnect(clientID)
	}
}

// NewHub initializes a new Hub.
func NewHub(
	options HubOptions,
	clientStore WebsocketClientStore,
	clientFactory WebsocketClientFactory,
) *Hub {
	hub := &Hub{
		options:       options,
		clients:       clientStore,
		clientFactory: clientFactory,
	}

	hub.connectHandler.Store(defaultConnectHandler)
	hub.disconnectHandler.Store(defaultDisconnectHandler)
	hub.receiveHandler.Store(defaultReceiveHandler)
	hub.errorHandler.Store(hub.wrapErrorHandler(defaultErrorHandler))

	return hub
}

// ServeHTTP implements http.Handler interface and responsible for connect new clients.
func (h *Hub) ServeHTTP(c *gin.Context) {
	if h.options.IsDebug {
		now := time.Now()
		defer func() {
			end := time.Since(now)
			if end > h.options.DebugFuncTimeLimit {
				//h.logger.Warnf("wspubsub.hub.connection_upgrade_handler: took=%s", end)
			}
		}()
	}

	receiveHandler := h.receiveHandler.Load().(ReceiveHandler)
	errorHandler := h.errorHandler.Load().(ErrorHandler)

	client := h.clientFactory.Create()
	client.OnReceive(receiveHandler)
	client.OnError(errorHandler)

	err := h.connectClient(client, c)
	if err != nil {
		http.Error(c.Writer, "Internal Server Error", http.StatusInternalServerError)
		if h.options.IsDebug {
			//h.logger.Errorf("cant upgrade connection: %s", err)
		}

		return
	}

	if h.options.IsDebug {
		//h.logger.Debugf("connection upgraded: id=%s", client.ID())
	}
}

// NewDefaultHub uses default dependencies to initializes a new hub.
func NewDefaultHub() *Hub {

	upgraderOptions := NewGorillaConnectionUpgraderOptions()
	upgrader := NewGorillaConnectionUpgrader(upgraderOptions)

	clientStoreOptions := NewClientStoreOptions()
	clientStore := NewClientStore(clientStoreOptions)

	clientOptions := NewClientOptions()
	clientFactory := NewClientFactory(clientOptions, upgrader)

	hubOptions := NewHubOptions()
	hub := NewHub(hubOptions, clientStore, clientFactory)

	return hub
}
