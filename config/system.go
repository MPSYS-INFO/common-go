package config

type System struct {
	Env           string `koanf:"env" json:"env" yaml:"env"`
	OssType       string `koanf:"oss-type" json:"oss-type" yaml:"oss-type"`
	RouterPrefix  string `koanf:"router-prefix" json:"router-prefix" yaml:"router-prefix"`
	Addr          int    `koanf:"addr" json:"addr" yaml:"addr"` // 端口值
	LimitCountIP  int    `koanf:"iplimit-count" json:"iplimit-count" yaml:"iplimit-count"`
	LimitTimeIP   int    `koanf:"iplimit-time" json:"iplimit-time" yaml:"iplimit-time"`
	UseMultipoint bool   `koanf:"use-multipoint" json:"use-multipoint" yaml:"use-multipoint"`
	UseRedis      bool   `koanf:"use-redis" json:"use-redis" yaml:"use-redis"`
}
