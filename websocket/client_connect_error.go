package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

// ClientConnectError returned when HTTP connection can't be upgraded to WebSocket connection.
type ClientConnectError struct {
	ID  uuid.UUID
	Err error
}

// ClientConnectError implements an error interface.
func (e *ClientConnectError) Error() string {
	return fmt.Sprintf("wspubsub: client failed to connect: id=%s, err=%s", e.ID, e.Err)
}

// NewClientConnectError initializes a new ClientConnectError.
func NewClientConnectError(id uuid.UUID, err error) *ClientConnectError {
	return &ClientConnectError{ID: id, Err: err}
}

// IsClientConnectError checks if error type is ClientConnectError.
func IsClientConnectError(err error) (*ClientConnectError, bool) {
	var v *ClientConnectError
	ok := errors.As(err, &v)

	return v, ok
}
