package core

import (
	"fmt"
	"os"

	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/providers/s3"
	"github.com/knadh/koanf/v2"
	"gitlab.com/MPSYS-INFO/common-go/core/internal"
)

func Koanf(conf any) *koanf.Koanf {
	var config string
	v := koanf.New(".")

	if config = os.Getenv(internal.ConfigEnv); config != "" {
		fmt.Printf("Используется значение, из файла :%s\n", config)
		if err := v.Load(file.Provider(config), yaml.Parser()); err != nil {
			panic(fmt.Errorf("Ошибка при загрузке конфигурационного файла: %s \n", err))
		}

	} else {
		fmt.Printf("Используется значение, s3")

		if err := v.Load(s3.Provider(s3.Config{
			Endpoint:  os.Getenv("S3_ENDPOINT"),
			AccessKey: os.Getenv("S3_ACCESS_KEY"),
			SecretKey: os.Getenv("S3_SECRET_KEY"),
			Region:    os.Getenv("S3_REGION"),
			Bucket:    os.Getenv("S3_BUCKET"),
			ObjectKey: os.Getenv("S3_OBJECT_KEY"),
		}), yaml.Parser()); err != nil {
			panic(fmt.Errorf("Ошибка при загрузке конфигурационного файла: %s \n", err))
		}
	}

	if err := v.Unmarshal("", conf); err != nil {
		fmt.Println(err)
	}
	return v
}
