package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

// ClientPingError returned when ping wsMessage can't be written to a WebSocket connection.
type ClientPingError struct {
	ID      uuid.UUID
	Message wsMessage
	Err     error
}

// ClientPingError implements an error interface.
func (e *ClientPingError) Error() string {
	return fmt.Sprintf("wspubsub: client failed to send a ping wsMessage: id=%s, err=%s", e.ID, e.Err)
}

// NewClientPingError initializes a new ClientPingError.
func NewClientPingError(id uuid.UUID, message wsMessage, err error) *ClientPingError {
	return &ClientPingError{ID: id, Message: message, Err: err}
}

// IsClientPingError checks if error type is ClientPingError.
func IsClientPingError(err error) (*ClientPingError, bool) {
	var v *ClientPingError
	ok := errors.As(err, &v)

	return v, ok
}
