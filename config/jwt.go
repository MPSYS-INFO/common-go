package config

type JWT struct {
	SigningKey  string `koanf:"signing-key" json:"signing-key" yaml:"signing-key"`    // jwt签名
	ExpiresTime string `koanf:"expires-time" json:"expires-time" yaml:"expires-time"` // 过期时间
	BufferTime  string `koanf:"buffer-time" json:"buffer-time" yaml:"buffer-time"`    // 缓冲时间
	Issuer      string `koanf:"issuer" json:"issuer" yaml:"issuer"`                   // 签发者
}
