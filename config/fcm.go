package config

type FCM struct {
	APIKey string `koanf:"api_key" json:"api_key" yaml:"api_key"`
}
