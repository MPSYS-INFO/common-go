package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

// ClientNotFoundError returned when client is not present in a storage.
type ClientNotFoundError struct {
	ID uuid.UUID
}

// ClientNotFoundError implements an error interface.
func (e *ClientNotFoundError) Error() string {
	return fmt.Sprintf("wspubsub: client not found: id=%s", e.ID)
}

// NewClientNotFoundError initializes a new ClientNotFoundError.
func NewClientNotFoundError(id uuid.UUID) *ClientNotFoundError {
	return &ClientNotFoundError{ID: id}
}

// IsClientNotFoundError checks if error type is ClientNotFoundError.
func IsClientNotFoundError(err error) (*ClientNotFoundError, bool) {
	var v *ClientNotFoundError
	ok := errors.As(err, &v)

	return v, ok
}
