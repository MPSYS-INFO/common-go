package websocket

import (
	"github.com/google/uuid"
	"net/http"
)

type WebsocketConnectionUpgrader interface {
	Upgrade(w http.ResponseWriter, r *http.Request) (WebsocketConnection, error)
}

type WebsocketConnection interface {
	Read() (wsMessage, error)
	Write(message wsMessage) error
	Close() error
}

type ClientFactory struct {
	options  ClientOptions
	upgrader WebsocketConnectionUpgrader
}

// Create returns a new client.
func (f *ClientFactory) Create() WebsocketClient {
	return NewClient(f.options, uuid.New(), f.upgrader)
}

// NewClientFactory initializes a new ClientFactory.
func NewClientFactory(
	options ClientOptions,
	upgrader WebsocketConnectionUpgrader,
) *ClientFactory {
	factory := &ClientFactory{
		options:  options,
		upgrader: upgrader,
	}

	return factory
}
