package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

// ClientRepeatConnectError returned when trying to connect an already connected client.
type ClientRepeatConnectError struct {
	ID uuid.UUID
}

// ClientRepeatConnectError implements an error interface.
func (e *ClientRepeatConnectError) Error() string {
	return fmt.Sprintf("wspubsub: client is already connected: id=%s", e.ID)
}

// NewClientRepeatConnectError initializes a new ClientRepeatConnectError.
func NewClientRepeatConnectError(id uuid.UUID) *ClientRepeatConnectError {
	return &ClientRepeatConnectError{ID: id}
}

// IsClientRepeatConnectError checks if error type is ClientRepeatConnectError.
func IsClientRepeatConnectError(err error) (*ClientRepeatConnectError, bool) {
	var v *ClientRepeatConnectError
	ok := errors.As(err, &v)

	return v, ok
}
