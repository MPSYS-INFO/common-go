package timer

import (
	"github.com/robfig/cron/v3"
	"sync"
)

type Timer interface {
	AddTaskByFunc(taskName string, spec string, task func(), autoStart bool, option ...cron.Option) (cron.EntryID, error)
	AddTaskByFuncWithSecond(taskName string, spec string, task func(), autoStart bool, option ...cron.Option) (cron.EntryID, error)
	AddTaskByJob(taskName string, spec string, job interface{ Run() }, autoStart bool, option ...cron.Option) (cron.EntryID, error)
	AddTaskByJobWithSeconds(taskName string, spec string, job interface{ Run() }, autoStart bool, option ...cron.Option) (cron.EntryID, error)
	FindCron(taskName string) (*cron.Cron, bool)
	StartTask(taskName string)
	StopTask(taskName string)
	Remove(taskName string, id int)
	Clear(taskName string)
	Close()
}

// timer
type timer struct {
	taskList map[string]*cron.Cron
	sync.Mutex
}

// AddTaskByFunc добавление задачи
func (t *timer) AddTaskByFunc(taskName string, spec string, task func(), autoStart bool, option ...cron.Option) (cron.EntryID, error) {
	t.Lock()
	defer t.Unlock()
	if _, ok := t.taskList[taskName]; !ok {
		t.taskList[taskName] = cron.New(option...)
	}
	id, err := t.taskList[taskName].AddFunc(spec, task)
	if autoStart {
		t.taskList[taskName].Start()
	}
	return id, err
}

// AddTaskByFuncWithSecond добавление задачи
func (t *timer) AddTaskByFuncWithSecond(taskName string, spec string, task func(), autoStart bool, option ...cron.Option) (cron.EntryID, error) {
	t.Lock()
	defer t.Unlock()
	option = append(option, cron.WithSeconds())
	if _, ok := t.taskList[taskName]; !ok {
		t.taskList[taskName] = cron.New(option...)
	}
	id, err := t.taskList[taskName].AddFunc(spec, task)
	if autoStart {
		t.taskList[taskName].Start()
	}

	return id, err
}

// AddTaskByJob добавление задачи
func (t *timer) AddTaskByJob(taskName string, spec string, job interface{ Run() }, autoStart bool, option ...cron.Option) (cron.EntryID, error) {
	t.Lock()
	defer t.Unlock()
	if _, ok := t.taskList[taskName]; !ok {
		t.taskList[taskName] = cron.New(option...)
	}
	id, err := t.taskList[taskName].AddJob(spec, job)
	if autoStart {
		t.taskList[taskName].Start()
	}
	return id, err
}

// AddTaskByJobWithSeconds добавление задачи
func (t *timer) AddTaskByJobWithSeconds(taskName string, spec string, job interface{ Run() }, autoStart bool, option ...cron.Option) (cron.EntryID, error) {
	t.Lock()
	defer t.Unlock()
	option = append(option, cron.WithSeconds())
	if _, ok := t.taskList[taskName]; !ok {
		t.taskList[taskName] = cron.New(option...)
	}
	id, err := t.taskList[taskName].AddJob(spec, job)
	if autoStart {
		t.taskList[taskName].Start()
	}
	return id, err
}

// FindCron поиск задачи по имени
func (t *timer) FindCron(taskName string) (*cron.Cron, bool) {
	t.Lock()
	defer t.Unlock()
	v, ok := t.taskList[taskName]
	return v, ok
}

// StartTask запуск по имени
func (t *timer) StartTask(taskName string) {
	t.Lock()
	defer t.Unlock()
	if v, ok := t.taskList[taskName]; ok {
		v.Start()
	}
}

// StopTask остановка выполнения
func (t *timer) StopTask(taskName string) {
	t.Lock()
	defer t.Unlock()
	if v, ok := t.taskList[taskName]; ok {
		v.Stop()
	}
}

// Remove удаление по имени и крон ид
func (t *timer) Remove(taskName string, id int) {
	t.Lock()
	defer t.Unlock()
	if v, ok := t.taskList[taskName]; ok {
		v.Remove(cron.EntryID(id))
	}
}

// Clear
func (t *timer) Clear(taskName string) {
	t.Lock()
	defer t.Unlock()
	if v, ok := t.taskList[taskName]; ok {
		v.Stop()
		delete(t.taskList, taskName)
	}
}

// Close
func (t *timer) Close() {
	t.Lock()
	defer t.Unlock()
	for _, v := range t.taskList {
		v.Stop()
	}
}

func NewTimerTask() Timer {
	return &timer{taskList: make(map[string]*cron.Cron)}
}
