package websocket

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

// ClientReceiveError returned when wsMessage can't be read from a WebSocket connection.
type ClientReceiveError struct {
	ID      uuid.UUID
	Message wsMessage
	Err     error
}

// ClientReceiveError implements an error interface.
func (e *ClientReceiveError) Error() string {
	return fmt.Sprintf("wspubsub: client failed to receieve a wsMessage: id=%s, err=%s", e.ID, e.Err)
}

// NewClientReceiveError initializes a new ClientReceiveError.
func NewClientReceiveError(id uuid.UUID, message wsMessage, err error) *ClientReceiveError {
	return &ClientReceiveError{ID: id, Message: message, Err: err}
}

// IsClientReceiveError checks if error type is ClientReceiveError.
func IsClientReceiveError(err error) (*ClientReceiveError, bool) {
	var v *ClientReceiveError
	ok := errors.As(err, &v)

	return v, ok
}
