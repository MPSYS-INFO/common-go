package config

type Sentry struct {
	Dsn string `koanf:"dsn" json:"dsn" yaml:"dsn"`
}
