package config

import (
	"log/slog"
	"strings"
)

type SLog struct {
	Level      string `koanf:"level" json:"level" yaml:"level"`    // Уровень
	Prefix     string `koanf:"prefix" json:"prefix" yaml:"prefix"` // Префикс
	ShowSource bool   `koanf:"show-source" json:"show-source" yaml:"show-source"`
}

func (s *SLog) TransportLevel() slog.Leveler {
	s.Level = strings.ToLower(s.Level)
	switch s.Level {
	case "debug":
		return slog.LevelDebug
	case "info":
		return slog.LevelInfo
	case "warn":
		return slog.LevelWarn
	case "error":
		return slog.LevelError
	default:
		return slog.LevelDebug
	}
}
