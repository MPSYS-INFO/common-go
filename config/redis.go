package config

type Redis struct {
	Addr     string `koanf:"addr" json:"addr" yaml:"addr"`             // 服务器地址:端口
	Password string `koanf:"password" json:"password" yaml:"password"` // 密码
	DB       int    `koanf:"db" json:"db" yaml:"db"`                   // redis的哪个数据库
}
