package config

type GeneralDB struct {
	Type         string `koanf:"type" json:"type" yaml:"type"`
	AliasName    string `koanf:"alias-name" json:"alias-name" yaml:"alias-name"`
	Prefix       string `koanf:"prefix" json:"prefix" yaml:"prefix"`
	Port         string `koanf:"port" json:"port" yaml:"port"`
	Config       string `koanf:"config" json:"config" yaml:"config"`
	Dbname       string `koanf:"db-name" json:"db-name" yaml:"db-name"`
	Username     string `koanf:"username" json:"username" yaml:"username"`
	Password     string `koanf:"password" json:"password" yaml:"password"`
	Path         string `koanf:"path" json:"path" yaml:"path"`
	Engine       string `koanf:"engine" json:"engine" yaml:"engine" default:"InnoDB"`
	LogMode      string `koanf:"log-mode" json:"log-mode" yaml:"log-mode"`
	MaxIdleConns int    `koanf:"max-idle-conns" json:"max-idle-conns" yaml:"max-idle-conns"`
	MaxOpenConns int    `koanf:"max-open-conns" json:"max-open-conns" yaml:"max-open-conns"`
	Singular     bool   `koanf:"singular" json:"singular" yaml:"singular"`
	Disable      bool   `koanf:"disable" json:"disable" yaml:"disable"`
}

func (p *GeneralDB) PgsqlDsn() string {
	return "host=" + p.Path + " user=" + p.Username + " password=" + p.Password + " dbname=" + p.Dbname + " port=" + p.Port + " " + p.Config
}
