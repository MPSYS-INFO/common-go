package core

import (
	"gitlab.com/MPSYS-INFO/common-go/config"

	"log/slog"
	"os"
)

func Slog(conf config.SLog) (logger *slog.Logger) {

	opt := &slog.HandlerOptions{AddSource: conf.ShowSource, Level: conf.TransportLevel()}
	handler := slog.NewJSONHandler(os.Stdout, opt)
	logger = slog.New(handler).With("service", conf.Prefix)
	return logger
}
