package config

type Casdoor struct {
	CasdoorUrl   string `koanf:"casdoor-url" json:"casdoor-url" yaml:"casdoor-url"`
	ClientId     string `koanf:"client-id" json:"client-id" yaml:"client-id"`
	ClientSecret string `koanf:"client-secret" json:"client-secret" yaml:"client-secret"`
}
