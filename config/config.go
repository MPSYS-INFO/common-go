package config

type Server struct {
	JWT          JWT         `koanf:"jwt" json:"jwt" yaml:"jwt"`
	Zap          SLog        `koanf:"slog" json:"slog" yaml:"slog"`
	Redis        Redis       `koanf:"redis" json:"redis" yaml:"redis"`
	System       System      `koanf:"system" json:"system" yaml:"system"`
	DBList       []GeneralDB `koanf:"db-list" json:"db-list" yaml:"db-list"`
	Timer        Timer       `koanf:"timer" json:"timer" yaml:"timer"`
	AMQP         AMQP        `koanf:"amqp" json:"amqp" yaml:"amqp"`
	Cors         CORS        `koanf:"cors" json:"cors" yaml:"cors"`
	FirebaseJson string      `koanf:"firebase-json" json:"firebase-json" yaml:"firebase-json"`
	Casdoor      Casdoor     `koanf:"casdoor" json:"casdoor" yaml:"casdoor"`
	Sentry       Sentry      `koanf:"sentry" json:"sentry" yaml:"sentry"`
}
